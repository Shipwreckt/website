## **Shipwreckt.co.uk Website Repository**

Welcome to my personal website repository! This project is licensed under the GNU Affero General Public License v3.0, giving you the freedom to copy, modify, and distribute it. Additionally, you are not required to credit this website as your template if you choose to use it.

### License

This project is licensed under the GNU Affero General Public License v3.0. You can view the full license [here](https://www.gnu.org/licenses/agpl-3.0.html). A copy of the license should be included in this repository.

### How to Clone

To clone this repository, use the following command:

From my git server
```bash
git clone git://shipwreckt.co.uk/website


From Gitlab
```bash
git clone https://gitlab.com/Shipwreckt/website

